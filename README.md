# GitLab Ultimate or Gold for Open Source Projects

We take our responsibility of open source stewardship very seriously (https://about.gitlab.com/2016/01/11/being-a-good-open-source-steward/ and https://about.gitlab.com/stewardship/).

GitLab exists today in large part thanks to the work of hundreds of thousands of open source contributors around the world. To give back to this community who gives us so much, we want to help teams be more efficient, secure, and productive. We believe the best way for them to achieve this is by using as many of the capabilities of GitLab as possible.

It has already been the case for years that that any public project on GitLab.com gets all Gold features. We are happy to now offer a complimentary license to GitLab Ultimate (self-hosted) or subscription to GitLab Gold (SaaS) to all open source projects.

## Here's how to apply

1.   Create a gitlab.com account for your open source project: https://gitlab.com/users/sign_in
1.   Edit this file and add an entry to the [Open source projects using GitLab Ultimate or Gold](#open-source-projects-using-gitlab-ultimate-or-gold) 
section at the bottom of this page (all lines required):

     ```
     ### Project name
     A short description of what you do and why.
     https://myawesomeproject.org
     Ultimate or Gold?
     ```

1.   Commit your changes to a new branch and start a new Merge Request.

## Requirements

To apply:
- You need to be a project lead or a core contributor for an active open source project.
- Your project needs to use an [OSI-accepted open source license](https://opensource.org/licenses/alphabetical#)
- Your project must not seek to make profit from the resulting project software.

If you or your company work on commercial projects, consider our [plans for businesses](https://about.gitlab.com/pricing/).
If you're not sure if your project meets these requirements, please [contact our support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447) for help.

We'll review all requests and accept them at our discretion. If accepted, your project will be listed below and we will contact you.

## License/subscription details

- You'll receive a 1 year license for GitLab Ultimate or subscription for GitLab Gold.
- Support is not included, but can be purchased for 95% off, at $4.95/user/month. [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) for that.
- Your license/subscription can be renewed each year if your project still meets the requirements.
   - [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) 30 days before your license/subscription ends.
- Licenses and subscriptions cannot be transferred or sold.

## Open source projects using GitLab Ultimate or Gold

    ### Augur
    Augur is an open-source, decentralized, peer-to-peer oracle and prediction market platform built on the Ethereum blockchain.
    https://www.augur.net/
    Gold
    
    ### GNU Mailman
    GNU Mailman is a free and open source mailing list manager. We use Gitlab for all our [development](https://gitlab.com/mailman) and Continuous Integration.
    https://list.org
    Ultimate

    ### Manjaro Linux
    Manjaro is a user-friendly Linux distribution based on the independently developed Arch operating system. We use Gitlab for all our [development](https://gitlab.manjaro.org) and Continuous Integration.
    https://manjaro.org
    Ultimate

    ### NOC
    NOC is web-scale Operation Support System (OSS) for telecom and service providers. Areas covered by NOC include Network Resource Inventory (NRI), IP Address Management (IPAM), Fault Management (FM), Performance Management (PM), Peering Managemen. System is developed by telecom professionals and for telecom professionals.
    https://nocproject.org/
    Ultimate

    ### eelo
    An open source mobile phone OS that respects user’s data privacy
    https://www.eelo.io/
    Ultimate

    ### Mastodon
    An open source decentralized social network based on open web protocols.
    https://joinmastodon.org
    Ultimate

    ### Ninja Forms
    An open source drag-and-drop form builder for WordPress
    https://ninjaforms.com/
    Ultimate

    ### CHVote
    CHVote is one of only two accredited electronic voting systems by the Federal Council in Switzerland.
    https://republique-et-canton-de-geneve.github.io/chvote-1-0
    Ultimate

    ### Aurora OSS
    An open source ecosystem to provide an alternate to Google Ecosystem. Currently we provide AuroraStore as an alternate to Google PlayStore (https://gitlab.com/AuroraOSS/AuroraStore)
    https://gitlab.com/AuroraOSS/
    Ultimate

    ### Chakra Linux
    A community-developed GNU/Linux distribution with an emphasis on KDE and Qt technologies, utilizing a unique half-rolling release model that allows users to enjoy the latest versions of the Plasma desktop and their favorite applications on top of a periodically updated system core.
    https://www.chakralinux.org
    Ultimate

    ### CiviCRM
    CiviCRM is a web-based Open Source contact relationship management (CRM) system. CiviCRM emphasizes communicating with individuals, community engagement, activism, outreach, managing contributions, and managing memberships.
    https://civicrm.org/
    Ultimate

    ### SECU
    SЁCU is a service that allows you to send password protected self-destructing data packages. Thus the recipient will have to provide a password in order to open a package. And once it is opened, it will no longer be available.
    https://secu.su/
    Ultimate

    ### Spack
    Spack is a package manager for supercomputers, used by HPC centers and developers worldwide.
    https://gitlab.com/spack/spack
    Ultimate

    ### Alchemy Viewer
    A client for SecondLife/OpenMetaverse protocol compatible virtual world platforms.
    https://www.alchemyviewer.org
    Ultimate

    ### dps8m
    The dps8m project is an open source collaboration to create an emulator for the Honeywell DPS8/M mainframe computer, with the goal of running th Multics operating system.
    Ultimate

    ### mvdsv
    MVDSV: a QuakeWorld server
    https://github.com/deurk/mvdsv
    Ultimate

    ### Personal Management System
    Personal Management System is a system used to showcase not only your own personal projects and accomplishments, but also serve as your resume and blog engine. A one stop shop for all your personal branding needs.
    https://repo.theoremforge.com/me/PMS
    Ultimate

    ### OpenAPI
    Rest API for Puppet & Ansible
    https://cyberox.org/rails/openapi
    Ultimate

    ### Ruetomel
    An educative project for CS students to learn about devops, based on a given stack (java, springboot, docker, git/gitlab, k8s)
    https://www.devops.pf or https://gitlab.com/teriiehina/ruetomel
    Ultimate

    ### Groovybot
    Groovy is a feature-rich Discord Bot. His main-feature is to play specific songs available on YouTube via high quality streaming.
    https://groovybot.xyz
    Ultimate

    ### owlo
    An open source ActivityPub utilizing microblogging and fiction platform.
    http://localtoast.net/
    Ultimate

    ### MathLibrary
    A Kotlin library to assist you in Multivariable Calculus, Linear Algebra, Electrostatics, and Quantum Computing.
    https://github.com/ethertyper/mathlibrary
    Ultimate

    ### Ansible - PostgresXL cluster
    The main goal of this project is to have an ansible installer for full Postgres-XL cluster with gtms, coordinators, masters and slaves. The other goal is to have tests using role provision docker, to check behavior in a real environment.
    https://gitlab.com/elrender/postgres-xl-cluster
    Ultimate

    ### CoCoMS - Construction Correspondence Management System
    CoCoMS is a simple Document Management System designed specifically for the management of correspondence generated during the execution of a construction project. CoCoMS is targeted at document controllers and key staff of a construction project.
    https://gitlab.com/chrmina/cocoms
    Ultimate

    ### lainradio.club
    Lainradio.club is an open-source static website using Hugo as the generator. It's purpose is to simply aggregate past [radio] events (usually bi-friday evenings) and other useful stuff.
    https://lainradio.club

    ### OpenGAG
    An alternative to 9gag. But 100% open source. Driven by the community a bit like this [awesome thing](https://gitlab.com/gitlab-org/gitlab-ce)
    No url yet;

    ### Minecordbot
    A powerful way to bridge Minecraft and Discord.
    https://minecordbot.cyr1en.com
    Ultimate
